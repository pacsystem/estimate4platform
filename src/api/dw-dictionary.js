import request from '@/utils/request'
import API from '@/api/index.js'

// 登录方法
export function getDictList (queryParams) {
  var pageSize = queryParams.size
  var current = queryParams.current
  var itemName = queryParams.itemName
  var itemCode = queryParams.itemCode
  var dataStatus = queryParams.dataStatus
  var dictParentId = queryParams.dictParentId

  if (typeof (pageSize) === 'undefined') {
    pageSize = queryParams.pageSize
  }

  const params = {
    pageSize,
    current,
    itemName,
    itemCode,
    dataStatus,
    dictParentId
  }
  return request({
    url: API.dictionaryList,
    method: 'get',
    params: params
  })
}

// 登录方法
export function getDataDictItemByCode (itemCode) {
  const params = {
    itemCode
  }
  return request({
    url: API.dictionaryGetDataDictItemByCode,
    method: 'get',
    params: params
  })
}

/**
 * 创建用户
 * @param data
 * @returns {*}
 */
export function saveDict (data) {
  return request({
    url: API.dictionaryCreate,
    method: 'post',
    data
  })
}

export function updateDict (data) {
  return request({
    url: API.dictionaryUpdate,
    method: 'post',
    data
  })
}

export function selectDictLabel (statusOptions, dataStatus) {
  var _dataStatus = dataStatus + ''
  for (var i = 0; i< statusOptions.length; i++) {
    if (statusOptions[i].itemValue === _dataStatus) {
      return statusOptions[i].itemName
    }
  }
  return dataStatus
}

/**
 * 检测业务类型是否存在
 * @param data
 * @returns {*}
 */
export function dwDictCheckExists (data) {
  return request({
    url: API.dictionaryCheckExists,
    method: 'post',
    data
  })
}
