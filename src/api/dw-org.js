import request from '@/utils/request'
import API from '@/api/index.js'

export function getOrgList (pageSize, current, status, orgName) {
  const params = {
    pageSize,
    current,
    status,
    orgName
  }
  return request({
    url: API.orgList,
    method: 'get',
    params
  })
}

export function orgCreate (data) {
  return request({
    url: API.orgCreate,
    method: 'post',
    data
  })
}

export function orgUpdate (data) {
  return request({
    url: API.orgUpdate,
    method: 'put',
    data
  })
}

export function orgDelete (data) {
  return request({
    url: API.orgDelete,
    method: 'delete',
    data
  })
}

export function switchStatus (id) {
  const params = {
    id
  }
  return request({
    url: API.switchStatus,
    method: 'get',
    params
  })
}
