import request from '@/utils/request'
import API from '@/api/index.js'

/**
 * 问卷列表
 * @param pageSize
 * @param current
 * @returns {*}
 */
export function dwSurveyList (pageSize, current, surveyName, surveyState, businessType) {
  const params = {
    pageSize,
    current,
    surveyName,
    surveyState,
    businessType
  }
  return request({
    url: API.surveyList,
    method: 'get',
    params
  })
}
/**
 * 创建问卷
 * @param data
 * @returns {*}
 */
export function dwSurveyCreate (data) {
  return request({
    url: API.surveyCreate,
    method: 'post',
    data
  })
}

/**
 * 更新模板
 * @param data
 * @returns {*}
 */
export function dwSurveyTempUpdate (data) {
  return request({
    url: API.surveyTempUpdate,
    method: 'post',
    data
  })
}

/**
 * 更新问卷状态
 * @param surveyId
 * @param surveyState
 * @returns {*}
 */
export function dwSurveyUpState (surveyId, surveyState) {
  const params = {
    surveyId,
    surveyState
  }
  return request({
    url: API.surveyUpState,
    method: 'post',
    params
  })
}
/**
 * 复制问卷
 * @param data
 * @returns {*}
 */
export function dwSurveyCopy (fromSurveyId, surveyName, templateNumber) {
  const params = {
    fromSurveyId,
    surveyName,
    templateNumber,
    tag: '2'
  }
  return request({
    url: API.surveyCopy,
    method: 'post',
    params
  })
}
/**
 * 获取问卷信息
 * @param surveyId
 * @returns {*}
 */
export function dwSurveyInfo (surveyId) {
  const params = {
    id: surveyId
  }
  return request({
    url: API.surveyInfo,
    method: 'get',
    params
  })
}
/**
 * 更新问卷信息
 * @param data
 * @returns {*}
 */
export function dwSurveyUpdate (data) {
  return request({
    url: API.surveyUpdate,
    method: 'put',
    data
  })
}
/**
 * 更新问卷信息
 * @param data
 * @returns {*}
 */
export function dwSurveyDelete (data) {
  return request({
    url: API.surveyDelete,
    method: 'delete',
    data
  })
}
/**
 * 获取问卷的统计信息
 * @param surveyId
 * @returns {*}
 */
export function dwSurveyReport (surveyId) {
  const params = {
    surveyId
  }
  return request({
    url: API.surveyReport,
    method: 'get',
    params
  })
}
/**
 * 获取问卷的答卷列表
 * @param pageSize
 * @param current
 * @param surveyId
 * @returns {*}
 */
export function dwSurveyAnswerList (pageSize, current, surveyId) {
  const params = {
    pageSize,
    current,
    surveyId
  }
  return request({
    url: API.surveyAnswerList,
    method: 'get',
    params
  })
}

/**
 * 获取问卷的答卷列表
 * @param pageSize
 * @param current
 * @param surveyId
 * @returns {*}
 */
export function dwSurveyAnswerAllList (pageSize, current, surveyId, businessName, status, businessId, startToEndEstimateDate, estimateStatus, orgCode, estimatePerson) {
  const params = {
    pageSize,
    current,
    surveyId,
    // startToEndDate,
    businessId,
    startToEndEstimateDate,
    estimateStatus,
    orgCode,
    estimatePerson
  }
  return request({
    url: API.surveyAnswerAllList,
    method: 'get',
    params
  })
}

/**
 * 更新问卷信息
 * @param data
 * @returns {*}
 */
export function dwSurveyAnswerDelete (data) {
  return request({
    url: API.surveyAnswerDelete,
    method: 'delete',
    data
  })
}
/**
 * 获取问卷答卷详情
 * @param answerId
 * @returns {*}
 */
export function dwSurveyAnswerInfo (answerId, sourceNumber) {
  const data = {
    answerId: answerId,
    sourceNumber: sourceNumber
  }
  return request({
    url: API.surveyAnswerInfo,
    method: 'post',
    data
  })
}

/**
 * 检测业务类型是否存在
 * @param data
 * @returns {*}
 */
export function dwSurveyCheckExists (data) {
  return request({
    url: API.surveyCheckExists,
    method: 'post',
    data
  })
}

/**
 * 根据业务类型获取所有列表无分页
 * @param data
 * @returns
 */
export function getSurverList (ids) {
  const params = {ids}
  return request({
    url: API.surverList,
    method: 'get',
    params: params
  })
}

export function dwSurveyAnswerOrgSimpleList () {
  return request({
    url: API.getOrgSimpleList,
    method: 'get'
  })
}
