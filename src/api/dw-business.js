import request from '@/utils/request'
import API from '@/api/index.js'

// 登录方法
export function dwBusinessManageList (pageSize, current, businessName, status) {
  // var jsonbm = JSON.stringify(businessManage)
  const params = {
    pageSize,
    current,
    status,
    businessName
  }
  return request({
    url: API.businessManageList,
    method: 'get',
    params: params
  })
}

/**
 * 创建用户
 * @param data
 * @returns {*}
 */
export function dwBusinessManageCreate (data) {
  return request({
    url: API.businessManageCreate,
    method: 'post',
    data
  })
}

export function dwBusinessManageUpdate (data) {
  return request({
    url: API.businessManagerUpdate,
    method: 'post',
    data
  })
}

/**
 * 物理删除业务类型
 * @param data
 */
export function dwBusinessManageDelete (data) {
  return request({
    url: API.businessManageDelete,
    method: 'delete',
    data
  })
}

/**
 * 逻辑删除业务类型
 * @param data
 */
export function dwBusinessManageDisable (data) {
  return request({
    url: API.businessManageDisable,
    method: 'put',
    data
  })
}

/**
 * 检测业务类型是否存在
 * @param data
 * @returns {*}
 */
export function dwBusinessManageCheckExists (data) {
  return request({
    url: API.businessManageCheckExists,
    method: 'post',
    data
  })
}

/**
 * 检测业务类型是否存在
 * @param data
 * @returns
 */
export function dwBusinessManageNvMenuList (data) {
  const params = {
  }
  return request({
    url: API.businessManageNvMenuList,
    method: 'get',
    params: params
  })
}

/**
 * 检测该业务类型下是否有模板
 * @param data
 * @returns
 */
export function checkDwSurveyListByBusinessType (id) {
  const params = {id}
  return request({
    url: API.getDwSurveyListByBusinessType,
    method: 'get',
    params: params
  })
}
