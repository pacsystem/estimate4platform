import axios from 'axios'
// import urlConfig from '@/urlConfig'

axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8'
// 创建axios实例
const service = axios.create({
  // axios中请求配置有baseURL选项，表示请求URL公共部分
  // baseURL: urlConfig.tdfURL,
  baseURL: window.urlConfig.tdfURL,
  // 超时
  timeout: 10000
})

// 响应拦截器
service.interceptors.response.use(res => {
  return res.data
},
error => {
  return Promise.reject(error)
}
)

// 查询平台当前用户信息
export const ssoGetUserInfo = (encryptStr) => {
  return service({
    url: 'tdf-service-sys/decrypt/skipInfo/get?encryptStr=' + encryptStr,
    method: 'get'
  })
}
