import router from './router'
import DwAuthorized from '@/utils/dw-authorized'
import {ssoGetUserInfo} from '@/api/dw-sso'
import {notLogin} from '@/api/dw-login'
import {msgError} from '@/utils/dw-msg'
const whiteList = ['/no-top/dw/thirdDwSurveyAnswer']

router.beforeEach((to, from, next) => {
  const href = window.location.href
  // setTimeout(() =>{
  if (href.indexOf('encryptStr') > -1) {
    console.log('encryptStr')
    // 查询用户信息
    const encryptStr = location.href.split('?')[1].split('=')[1]
    ssoGetUserInfo(encryptStr).then((res) => {
      if (res.data) {
        const userCode = res.data.userCode
        notLogin(userCode).then(res => {
          if (res.data.status === 'ok') {
            DwAuthorized.setAuthority(res.data.currentAuthority)
            DwAuthorized.setUserName(userCode)
            window.location.replace(location.href.split('?')[0])
            window.location.reload()
          } else {
            if (res.data.hasOwnProperty('httpResult') && res.data.httpResult!=null && res.data.httpResult.hasOwnProperty('resultMsg')) {
              msgError(res.data.httpResult.resultMsg)
            } else {
              msgError('登录失败，请确认！')
            }
          }
        })
      } else {
        console.log('GetUserInfo 失败', res)
      }
    }).catch(err => {
      console.log('平台跳转异常', err)
    })
  } else {
    const authority = DwAuthorized.getAuthority()
    if (whiteList.indexOf(to.path) !== -1) {
      // 在免登录白名单，直接进入
      next()
    } else {
      if (authority && authority.length > 0) {
        next()
      } else {
        // 跳出循环
        to.path === '/login' ? next() : next(`/login?redirect=${to.fullPath}`)
      }
    }
  }
  // },1000 *3)
})

router.afterEach(() => {
})
