import {Message, MessageBox} from 'element-ui'
import DwAuthorized from '@/utils/dw-authorized'

export function msgError (message) {
  Message({
    message: message,
    type: 'error',
    duration: 5 * 1000
  })
}
export function msgInfo (message) {
  Message({
    message: message,
    type: 'info',
    duration: 5 * 1000
  })
}

export function msgBoxNoLogin () {
  MessageBox.confirm('未登录状态，是否重新登录', '系统提示', {
    confirmButtonText: '重新登录',
    cancelButtonText: '取消',
    type: 'warning'
  }
  ).then(() => {
    window.location.href = process.env.DW_PUBLIC_PATH + '/#/login'
    DwAuthorized.clearLoginData()
  }).catch(() => {})
}

export function msgBoxNoRole () {
  MessageBox.confirm('账号没有相关操作权限', '系统提示', {
    confirmButtonText: '确认',
    showCancelButton: false,
    showClose: false,
    type: 'warning'
  }
  ).then(() => {
    window.location.href = process.env.DW_PUBLIC_PATH + '/#/'
  }).catch(() => {})
}
