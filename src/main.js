// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import axios from 'axios'
import VueAxios from 'vue-axios'
import 'element-ui/lib/theme-chalk/index.css'
import 'normalize.css'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import './utils/dw-common'
import '@/assets/styles/index.css' // global css
import './permission' // permission control

Vue.prototype.$urlConfig = window.urlConfig
Vue.config.productionTip = false

Vue.use(ElementUI)
Vue.use(VueAxios, axios)
Vue.component('icon', Icon)

console.log(process.env.DW_PUBLIC_PATH)
sessionStorage.setItem('DW_PUBLIC_PATH', process.env.DW_PUBLIC_PATH)
sessionStorage.setItem('DW_API_URL', process.env.DW_API_URL)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {App},
  template: '<App/>'
})
