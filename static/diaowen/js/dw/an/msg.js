var DW_API_URL = sessionStorage.getItem('DW_API_URL');
var DW_PUBLIC_PATH = sessionStorage.getItem('DW_PUBLIC_PATH');

$(document).ready(function(){
    if((DW_API_URL === null) || (DW_PUBLIC_PATH === null))
    {
      var res = getRootPathD();
      if(res.AppName ==='')
      {
        // DW_API_URL = res.ApiUrl;
        DW_API_URL = '/dev-api';
        DW_PUBLIC_PATH = '';
        sessionStorage.setItem('DW_API_URL',DW_API_URL);
        sessionStorage.setItem('DW_PUBLIC_PATH',DW_PUBLIC_PATH);
      }
      else
      {
        DW_API_URL = '/estimate-api';
        DW_PUBLIC_PATH = '/estimate4platform';
        sessionStorage.setItem('DW_API_URL',DW_API_URL);
        sessionStorage.setItem('DW_PUBLIC_PATH',DW_PUBLIC_PATH);
      }
    }
    var sid = $.getUrlParam("sid");
    var respType = $.getUrlParam("respType");
    var answerId = $.getUrlParam("answerId");
    var pwdCode = $.getUrlParam("pwdCode");
    var urlParams = getUrlVars();
    // var estimateName = decodeURI($.getUrlParam("estimateName"));
    // var estimateName = decodeURI(urlParams.estimateName);
    // var estimateGender = decodeURI($.getUrlParam("estimateGender"));
    // var estimateAge = decodeURI($.getUrlParam("estimateAge"));
    // var outpatientNumber = decodeURI($.getUrlParam("outpatientNumber"));
    // var hospitalNumber = decodeURI($.getUrlParam("hospitalNumber"));
    // var inspectNumber = decodeURI($.getUrlParam("inspectNumber"));
    var source = decodeURI($.getUrlParam("source"));
    var sourceNumber = decodeURI($.getUrlParam("sourceNumber"));

    var estimatePerson = decodeURI(urlParams.estimatePerson);
    var orgName = decodeURI(urlParams.orgName);
    var orgCode = decodeURI(urlParams.orgCode);
    var estimateTemplateName = decodeURI(urlParams.estimateTemplateName);
    var estimateDate = decodeURI(urlParams.estimateDate);
    var estimateScore = decodeURI(urlParams.estimateScore);
    var estimateStatus = decodeURI(urlParams.estimateStatus);

  if (typeof (estimateTemplateName) === 'undefined') {
    estimateTemplateName = ''
  }
  if (typeof (estimateScore) === 'undefined') {
    estimateScore = ''
  }
  if (typeof (estimateStatus) === 'undefined') {
    estimateStatus = ''
  }
  if (typeof (estimateDate) === 'undefined') {
    estimateDate = ''
  }

    // $("#estimateName").val(estimateName);
    // $("#estimateGender").val(estimateGender);
    // $("#estimateAge").val(estimateAge);
    // $("#outpatientNumber").val(outpatientNumber);
    // $("#hospitalNumber").val(hospitalNumber);
    // $("#inspectNumber").val(inspectNumber);
    $("#source").val(source);
    $("#sourceNumber").val(sourceNumber);

    $("#estimatePerson").val(estimatePerson);
    $("#orgName").val(orgName);
    $("#orgCode").val(orgCode);
    $("#estimateTemplateName").val(estimateTemplateName);
    $("#estimateDate").val(estimateDate);
    $("#estimateScore").val(estimateScore);
    $("#estimateStatus").val(estimateStatus);
    // $("#answerId").val(answerId);

    resultStatus2Msg(respType,sid,pwdCode);
    querySurveyData(respType,sid);
    $("#surveyAnswerButton").click(function(){
        var newUrl =$("#surveyAnswerForm").attr('action');
        $("#surveyAnswerForm").attr('action', DW_PUBLIC_PATH + newUrl);
        $("#surveyAnswerForm").submit();
    });
});

function getUrlVars() {
  var vars = [], hash;
  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  for (var i = 0; i < hashes.length; i++) {
    hash = hashes[i].split('=');
    vars.push(hash[0]);
    vars[hash[0]] = hash[1];
  }
  return vars;
}

function getRootPathD() {
  // 1、获取当前全路径，如： http://localhost:8080/springmvc/page/frame/test.html
  var curWwwPath = window.location.href;
  // 获取当前相对路径： /springmvc/page/frame/test.html
  var pathName = window.location.pathname;    // 获取主机地址,如： http://localhost:8080
  var local = curWwwPath.substring(0,curWwwPath.indexOf(pathName));
  // 获取带"/"的项目名，如：/springmvc
  var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
  var rootPath = projectName;
  if(rootPath.indexOf('static') != -1)
    rootPath = '';D
  var res ={};
  res.ApiUrl = local;
  res.AppName = rootPath;
  return res;
}

function resultStatus2Msg(resptype,sid,ruleCode) {
  var tempMsg = {};
  tempMsg.success = false;
  if(resptype==='1'){
    tempMsg.resultNote = '目前该评价已暂停收集，请稍后再试!';
    tempMsg.resultColor = "#e70f0f";
  }else if(resptype==='2'){
    tempMsg.resultNote = '目前该评价无法访问，请稍后再试!';
    tempMsg.resultColor = "#e70f0f";
  }else if(resptype==='3'){
    tempMsg.resultNote = '已经提交过此评价，请不要重复作答!';
    tempMsg.resultColor = "#e70f0f";
  }else if(resptype==='4'){
    tempMsg.resultNote = '验证码不正确，操作未成功!';
    tempMsg.resultColor = "#e70f0f";
    setReqUrl({reqUrl:DW_PUBLIC_PATH + '/static/diaowen/answer-p.html?sid='+sid,urlText:'重新评价'})
  }else if(resptype==='5'){
    tempMsg.resultNote = '发生未知异常，操作未成功!';
    tempMsg.resultColor = "#e70f0f";
  }else if(resptype==='6'){
    tempMsg.resultNote = '评价提交成功，感谢您的支持!';
    tempMsg.resultColor = "#1890ff";
    tempMsg.success = true;
  }else if(resptype==='7'){
    tempMsg.resultNote = '评价已经达到收集上限，感谢您的支持!（数据不被保存）';
    tempMsg.resultColor = "#e70f0f";
  }else if(resptype==='8'){
    tempMsg.resultNote = '评价未到开始时间，感谢您的支持!（数据不被保存）';
    tempMsg.resultColor = "#e70f0f";
  }else if(resptype==='9'){
    tempMsg.resultNote = '评价已经到了截止时间，感谢您的支持!（数据不被保存）';
    tempMsg.resultColor = "#e70f0f";
  }else if(resptype==='10'){
    tempMsg.resultNote = '该评价已删除，无法作答!';
    tempMsg.resultColor = "#e70f0f";
  }else if(resptype==='21'){
    tempMsg.resultNote = '不在本次调研范围内，无法作答!';
    tempMsg.resultColor = "#e70f0f";
  }else if(resptype==='23'){
    tempMsg.resultNote = '超过单个IP评价次数限制!';
    tempMsg.resultColor = "#e70f0f";
  }else if(resptype==='106'){
    tempMsg.resultNote = '口令超过使用次数!';
    tempMsg.resultColor = "#e70f0f";
  }else if(resptype==='107'){
    tempMsg.resultNote = '系统开启参数校验,缺失参数!';
    tempMsg.resultColor = "#e70f0f";
  }else if(resptype==='108'){
    tempMsg.resultNote = '评价已过收集期限!';
    tempMsg.resultColor = "#e70f0f";
  }else if(resptype==='302' || resptype==='303'){
    if(resptype==='302') {
      tempMsg.resultNote = '口令码错误!';
    }else{
      tempMsg.resultNote = '需要口令才可开始评价!';
    }
    tempMsg.resultColor = "#e70f0f";
    $("input[name='sid']").val(sid);
    $("#ruleCodeDiv").show();
  }else if(resptype==='201'){
    var qrSrc = DW_API_URL+ "/api/dwsurvey/anon/response/answerTD.do?sid="+sid;
    if(ruleCode!==undefined && ruleCode!==""){
      qrSrc = DW_API_URL + "/api/dwsurvey/anon/response/answerTD.do?sid="+sid+"&ruleCode="+ruleCode;
    }
    tempMsg.resultNote = "<div><img className=\"mobileAnswerQR\" src="+qrSrc+" style=\"padding:0,background:'white'\" /><div style=\"padding:10\">请使用微信扫码评价</div></div>";
    tempMsg.resultColor = "#1890ff";
  }else if(resptype==='202'){
    tempMsg.resultNote = '超过有效评价次数!';
    tempMsg.resultColor = "#e70f0f";
  }
  tempMsg.respType = resptype;
  setResultMsg(tempMsg);
}

function setResultMsg(tempMsg){
  $("#resultNote").html(tempMsg.resultNote);
  $("#resultNote").css("color",tempMsg.resultColor);
  $("#respType").html("状态码："+tempMsg.respType);
}

function querySurveyData(respType,sid){
  if (sid != null) {
    querySurvey(sid,function(httpResult){
      if(httpResult!=null && httpResult.hasOwnProperty('resultCode') && httpResult.resultCode === 200 ){
        var resultData = httpResult.data;
        $("#surveyName").html(resultData.surveyName);
      }
    });
  }
}

function querySurvey(sid,callback){
  var url = DW_API_URL + '/api/dwsurvey/anon/response/survey_info.do';
  var data = "sid="+sid;
  $.ajax({
    url:url,
    data:data,
    // type:"json",
    success:function(httpResult){
      // console.debug(httpResult);
      if(callback!=null){
        callback(httpResult);
      }
    }
  });
}

function setReqUrl(reqObj){
  var reqUrlDiv = $("#reqUrlDiv");
  reqUrlDiv.empty();
  reqUrlDiv.append("<a href=\""+reqObj.reqUrl+"\">"+reqObj.urlText+"</a>");
}
