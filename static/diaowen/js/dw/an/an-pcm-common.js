var DW_API_URL = sessionStorage.getItem('DW_API_URL');
var DW_PUBLIC_PATH = sessionStorage.getItem('DW_PUBLIC_PATH');

function checkAnswerSurvey(sid){

  if((DW_API_URL === null) || (DW_PUBLIC_PATH === null))
  {
    var res = getRootPathE();
    if(res.AppName ==='')
    {
      // DW_API_URL = res.ApiUrl;
      DW_API_URL = '/dev-api';
      DW_PUBLIC_PATH = '';
      sessionStorage.setItem('DW_API_URL',DW_API_URL);
      sessionStorage.setItem('DW_PUBLIC_PATH',DW_PUBLIC_PATH);
    }
    else
    {
      DW_API_URL = '/estimate-api';
      DW_PUBLIC_PATH = '/estimate4platform';
      sessionStorage.setItem('DW_API_URL',DW_API_URL);
      sessionStorage.setItem('DW_PUBLIC_PATH',DW_PUBLIC_PATH);
    }
  }

  var ctx=$("#ctx").val();
  var ruleCode = $("#ruleCode").val();
  var wxCode = $("#wxCode").val();
  // var estimateName = $("#estimateName").val();
  // var estimateGender = $("#estimateGender").val();
  // var estimateAge = $("#estimateAge").val();
  // var outpatientNumber = $("#outpatientNumber").val();
  // var hospitalNumber = $("#hospitalNumber").val();
  // var inspectNumber = $("#inspectNumber").val();
  var source = $("#source").val();
  var sourceNumber = $("#sourceNumber").val();
  var estimatePerson = $("#estimatePerson").val();
  var orgName = $("#orgName").val();
  var orgCode = $("#orgCode").val();
  var estimateTemplateName = $("#estimateTemplateName").val();
  var estimateDate = $("#estimateDate").val();
  var estimateScore = $("#estimateScore").val();
  var estimateStatus = $("#estimateStatus").val();
  var endDiagTime = $("#endDiagTime").val()
  if(typeof (estimateTemplateName) === 'undefined')
  {
    estimateTemplateName = '';
  }
  if(typeof (estimateScore) === 'undefined')
  {
    estimateScore = '';
  }
  if(typeof (estimateStatus) === 'undefined')
  {
    estimateStatus = '';
  }
  if(typeof (estimateDate) === 'undefined')
  {
    estimateDate = '';
  }

  // var answerId = $("#answerId").val();
  //var lastParams = "&estimateName=" + estimateName + "&estimateGender=" + estimateGender +"&estimateAge=" + estimateAge+"&outpatientNumber=" + outpatientNumber+"&hospitalNumber=" + hospitalNumber+"&inspectNumber=" + inspectNumber+"&source=" + source+"&sourceNumber=" + sourceNumber;



  var lastParams = "&orgName=" + orgName + "&estimatePerson=" + estimatePerson +"&estimateTemplateName=" + estimateTemplateName+"&estimateDate=" + estimateDate+"&estimateScore="
    + estimateScore+"&estimateStatus=" + estimateStatus+"&source=" + source+"&sourceNumber=" + sourceNumber
    // +"&answerId=" + answerId
    + "&orgCode=" + orgCode + "&endDiagTime=" + endDiagTime;
  console.log(lastParams)
  var url=DW_API_URL + ctx+"/response/check-answer-survey.do";
  var data="surveyId="+$("#id").val()+"&ruleCode="+ruleCode+"&wxCode="+wxCode;
  $.ajax({
    url:url,
    data:data,
    type:"post",
    success:function(httpResult){
      // console.debug(httpResult);
      if(httpResult.resultCode==200){
        var data = httpResult.data;
        var answerCheck=data.answerCheck;
        var answerCheckCode=data.answerCheckCode;
        var imgCheckCode=data.imgCheckCode;
        if(!answerCheck){
          if(answerCheckCode===203){
            //跳转到指定地址
            window.location.href=data.redirectUrl;
          }else{
            //跳转到详情页
            if(ruleCode!=undefined && ruleCode!=""){
              // window.location.href="/#/diaowen-msg-code/"+sid+"/"+answerCheckCode+"/"+ruleCode;
              window.location.href=DW_PUBLIC_PATH + "/static/diaowen/message.html?sid="+sid+"&respType="+answerCheckCode+"&pwdCode="+ruleCode + lastParams;
            }else{
              // window.location.href="/#/diaowen-msg/"+sid+"/"+answerCheckCode;
              window.location.href=DW_PUBLIC_PATH + "/static/diaowen/message.html?sid="+sid+"&respType="+answerCheckCode + lastParams;
            }
          }
        }
        if(imgCheckCode){
          $("#jcaptchaImgBody").show();
          var ctx = $("#ctx").val();
          $("#register-jcaptchaImg").attr("src",DW_API_URL + ctx+"/jcap/jcaptcha.do");
        }
      }
    }
  });
}
function getUrlVars() {
  var vars = [], hash;
  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  for (var i = 0; i < hashes.length; i++) {
    hash = hashes[i].split('=');
    vars.push(hash[0]);
    vars[hash[0]] = hash[1];
  }
  return vars;
}
function getRootPathE() {
  // 1、获取当前全路径，如： http://localhost:8080/springmvc/page/frame/test.html
  var curWwwPath = window.location.href;
  // 获取当前相对路径： /springmvc/page/frame/test.html
  var pathName = window.location.pathname;    // 获取主机地址,如： http://localhost:8080
  var local = curWwwPath.substring(0,curWwwPath.indexOf(pathName));
  // 获取带"/"的项目名，如：/springmvc
  var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
  var rootPath = projectName;
  if(rootPath.indexOf('static') != -1)
    rootPath = '';
  var res ={};
  res.ApiUrl = local;
  res.AppName = rootPath;
  return res;
}
function refreshAutoCode(codeImgId){
  var ctx = $("#ctx").val();
  $("#"+codeImgId).attr("src",DW_API_URL + ctx+"/jcap/jcaptcha.do");
}
