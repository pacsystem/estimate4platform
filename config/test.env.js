'use strict'
const merge = require('webpack-merge')
const devEnv = require('./dev.env')

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"',
  DW_PUBLIC_PATH: '"/estimate4platform"',
  DW_API_URL: '"/estimate-api"',
  // DW_WEB_URL: '"http://localhost:8081"',
  // DW_RESOURCE_URL: '"http://localhost:8080"'
})
